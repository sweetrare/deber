public class FactoryProforma{
    public static Proforma getProforma(String tipo){
        if(tipo.equals("descuento")){

            return new ProformaDescuento();
        }
        else{
            return new ProformaDescuentoReducido();
        }
    }
}