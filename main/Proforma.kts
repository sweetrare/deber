public abstract class Proforma{
    private var proforma_id : Int;
    private var valor: Double;

    public fun getId: Int(){
        return this.proforma_id;
    }

    public fun setId(proforma_id: Int){
        this.proforma_id = proforma_id;
    }

    public fun getValor: Double(){
        return this.valor;
    }

    public fun setValor(valor:Double){
        this.valor = valor;
    }

    public abstract getValor:Double();

}